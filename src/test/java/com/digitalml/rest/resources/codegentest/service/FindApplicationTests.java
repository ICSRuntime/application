package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Application.ApplicationServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.FindApplicationInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.FindApplicationReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class FindApplicationTests {

	@Test
	public void testOperationFindApplicationBasicMapping()  {
		ApplicationServiceDefaultImpl serviceDefaultImpl = new ApplicationServiceDefaultImpl();
		FindApplicationInputParametersDTO inputs = new FindApplicationInputParametersDTO();
		inputs.setOffset(0);
		inputs.setPagesize(0);
		FindApplicationReturnDTO returnValue = serviceDefaultImpl.findapplication(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}