package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Application.ApplicationServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.CreateApplicationInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.CreateApplicationReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateApplicationTests {

	@Test
	public void testOperationCreateApplicationBasicMapping()  {
		ApplicationServiceDefaultImpl serviceDefaultImpl = new ApplicationServiceDefaultImpl();
		CreateApplicationInputParametersDTO inputs = new CreateApplicationInputParametersDTO();
		inputs.setRequest(new com.digitalml.rest.resources.codegentest.Request());
		CreateApplicationReturnDTO returnValue = serviceDefaultImpl.createapplication(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}