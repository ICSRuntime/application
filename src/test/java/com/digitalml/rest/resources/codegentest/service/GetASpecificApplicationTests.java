package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Application.ApplicationServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.GetASpecificApplicationInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.GetASpecificApplicationReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetASpecificApplicationTests {

	@Test
	public void testOperationGetASpecificApplicationBasicMapping()  {
		ApplicationServiceDefaultImpl serviceDefaultImpl = new ApplicationServiceDefaultImpl();
		GetASpecificApplicationInputParametersDTO inputs = new GetASpecificApplicationInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		GetASpecificApplicationReturnDTO returnValue = serviceDefaultImpl.getaspecificapplication(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}