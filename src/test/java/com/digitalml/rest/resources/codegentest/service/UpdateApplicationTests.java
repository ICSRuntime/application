package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Application.ApplicationServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.UpdateApplicationInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.UpdateApplicationReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class UpdateApplicationTests {

	@Test
	public void testOperationUpdateApplicationBasicMapping()  {
		ApplicationServiceDefaultImpl serviceDefaultImpl = new ApplicationServiceDefaultImpl();
		UpdateApplicationInputParametersDTO inputs = new UpdateApplicationInputParametersDTO();
		inputs.setRequest(new com.digitalml.rest.resources.codegentest.Request());
		UpdateApplicationReturnDTO returnValue = serviceDefaultImpl.updateapplication(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}