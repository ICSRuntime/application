package com.digitalml.insurance.application;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Application:
{
  "type": "object",
  "properties": {
    "id": {
      "type": "string"
    },
    "applicationJurisdiction": {
      "type": "string"
    },
    "applicationCountry": {
      "type": "string"
    },
    "applicationCounty": {
      "type": "string"
    },
    "applicationCollectionDate": {
      "type": "string",
      "format": "date"
    },
    "signedDate": {
      "type": "string",
      "format": "date"
    },
    "hoAssignedAppNumber": {
      "type": "string"
    },
    "attachment": {
      "type": "array",
      "items": {
        "$ref": "Attachment"
      }
    }
  }
}
*/

public class Application {

	@Size(max=1)
	private String id;

	@Size(max=1)
	private String applicationJurisdiction;

	@Size(max=1)
	private String applicationCountry;

	@Size(max=1)
	private String applicationCounty;

	@Size(max=1)
	private Date applicationCollectionDate;

	@Size(max=1)
	private Date signedDate;

	@Size(max=1)
	private String hoAssignedAppNumber;

	@Size(max=1)
	private List<com.digitalml.insurance.attachment.Attachment> attachment;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    id = null;
	    applicationJurisdiction = null;
	    applicationCountry = null;
	    applicationCounty = null;
	    
	    
	    hoAssignedAppNumber = null;
	    attachment = new ArrayList<com.digitalml.insurance.attachment.Attachment>();
	}
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getApplicationJurisdiction() {
		return applicationJurisdiction;
	}
	
	public void setApplicationJurisdiction(String applicationJurisdiction) {
		this.applicationJurisdiction = applicationJurisdiction;
	}
	public String getApplicationCountry() {
		return applicationCountry;
	}
	
	public void setApplicationCountry(String applicationCountry) {
		this.applicationCountry = applicationCountry;
	}
	public String getApplicationCounty() {
		return applicationCounty;
	}
	
	public void setApplicationCounty(String applicationCounty) {
		this.applicationCounty = applicationCounty;
	}
	public Date getApplicationCollectionDate() {
		return applicationCollectionDate;
	}
	
	public void setApplicationCollectionDate(Date applicationCollectionDate) {
		this.applicationCollectionDate = applicationCollectionDate;
	}
	public Date getSignedDate() {
		return signedDate;
	}
	
	public void setSignedDate(Date signedDate) {
		this.signedDate = signedDate;
	}
	public String getHoAssignedAppNumber() {
		return hoAssignedAppNumber;
	}
	
	public void setHoAssignedAppNumber(String hoAssignedAppNumber) {
		this.hoAssignedAppNumber = hoAssignedAppNumber;
	}
	public List<com.digitalml.insurance.attachment.Attachment> getAttachment() {
		return attachment;
	}
	
	public void setAttachment(List<com.digitalml.insurance.attachment.Attachment> attachment) {
		this.attachment = attachment;
	}
}