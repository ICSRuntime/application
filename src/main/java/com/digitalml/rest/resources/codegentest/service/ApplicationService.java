package com.digitalml.rest.resources.codegentest.service;
    	
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import java.net.URL;

import org.apache.commons.collections.CollectionUtils;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.SecurityContext;
import java.security.AccessControlException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.*;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

// Import any model objects used by the interface

import com.digitalml.rest.resources.codegentest.*;

/**
 * Service: Application
 * Access and update of insurance application information.
 *
 * This service has been automatically generated by Ignite
 *
 * @author admin
 * @version 1.0
 *
 */

public abstract class ApplicationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationService.class);

	// Required for JSR-303 validation
	static private ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

	protected static Mapper mapper;

	static {
		URL configFile = ApplicationService.class.getResource("ApplicationServiceMappings.xml");
		if (configFile != null) {

			List<String> mappingFiles = new ArrayList<String>();
			mappingFiles.add(configFile.toExternalForm());
			mapper = new DozerBeanMapper(mappingFiles);

		} else {
			mapper = new DozerBeanMapper(); // Use default wildcard mappings only
		}
	}

	protected boolean checkPermissions(SecurityContext securityContext) throws AccessControlException {
		return true;
	}

	/**
	Implements method getaspecificapplication
	
		Gets application details
	*/
	public GetASpecificApplicationReturnDTO getaspecificapplication(SecurityContext securityContext, GetASpecificApplicationInputParametersDTO inputs)  {

		if (LOGGER.isDebugEnabled())
			LOGGER.debug("Entered method getaspecificapplication");

		// Do any security checks
		if (securityContext == null)
			throw new AccessControlException("No SecurityContext available so cannot access getaspecificapplication");

		if (!checkPermissions(securityContext))
			throw new AccessControlException("Insufficient permissions to access getaspecificapplication");

		GetASpecificApplicationReturnDTO returnValue = new GetASpecificApplicationReturnDTO();
        GetASpecificApplicationCurrentStateDTO currentState = new GetASpecificApplicationCurrentStateDTO();
        
        // Setup the inputs for the first process step
        mapper.map(inputs, currentState.getInputs());
        
		Object returnDTO = null;
		return returnValue;
	}

	/**
	Implements method findapplication
	
		Gets a collection of application details filtered 
	*/
	public FindApplicationReturnDTO findapplication(SecurityContext securityContext, FindApplicationInputParametersDTO inputs)  {

		if (LOGGER.isDebugEnabled())
			LOGGER.debug("Entered method findapplication");

		// Do any security checks
		if (securityContext == null)
			throw new AccessControlException("No SecurityContext available so cannot access findapplication");

		if (!checkPermissions(securityContext))
			throw new AccessControlException("Insufficient permissions to access findapplication");

		FindApplicationReturnDTO returnValue = new FindApplicationReturnDTO();
        FindApplicationCurrentStateDTO currentState = new FindApplicationCurrentStateDTO();
        
        // Setup the inputs for the first process step
        mapper.map(inputs, currentState.getInputs());
        
		Object returnDTO = null;
		return returnValue;
	}

	/**
	Implements method updateapplication
	
		Updates application
	*/
	public UpdateApplicationReturnDTO updateapplication(SecurityContext securityContext, UpdateApplicationInputParametersDTO inputs)  {

		if (LOGGER.isDebugEnabled())
			LOGGER.debug("Entered method updateapplication");

		// Do any security checks
		if (securityContext == null)
			throw new AccessControlException("No SecurityContext available so cannot access updateapplication");

		if (!checkPermissions(securityContext))
			throw new AccessControlException("Insufficient permissions to access updateapplication");

		UpdateApplicationReturnDTO returnValue = new UpdateApplicationReturnDTO();
        UpdateApplicationCurrentStateDTO currentState = new UpdateApplicationCurrentStateDTO();
        
        // Setup the inputs for the first process step
        mapper.map(inputs, currentState.getInputs());
        
		Object returnDTO = null;
		return returnValue;
	}

	/**
	Implements method replaceapplication
	
		Replaces application
	*/
	public ReplaceApplicationReturnDTO replaceapplication(SecurityContext securityContext, ReplaceApplicationInputParametersDTO inputs)  {

		if (LOGGER.isDebugEnabled())
			LOGGER.debug("Entered method replaceapplication");

		// Do any security checks
		if (securityContext == null)
			throw new AccessControlException("No SecurityContext available so cannot access replaceapplication");

		if (!checkPermissions(securityContext))
			throw new AccessControlException("Insufficient permissions to access replaceapplication");

		ReplaceApplicationReturnDTO returnValue = new ReplaceApplicationReturnDTO();
        ReplaceApplicationCurrentStateDTO currentState = new ReplaceApplicationCurrentStateDTO();
        
        // Setup the inputs for the first process step
        mapper.map(inputs, currentState.getInputs());
        
		Object returnDTO = null;
		return returnValue;
	}

	/**
	Implements method createapplication
	
		Creates application
	*/
	public CreateApplicationReturnDTO createapplication(SecurityContext securityContext, CreateApplicationInputParametersDTO inputs)  {

		if (LOGGER.isDebugEnabled())
			LOGGER.debug("Entered method createapplication");

		// Do any security checks
		if (securityContext == null)
			throw new AccessControlException("No SecurityContext available so cannot access createapplication");

		if (!checkPermissions(securityContext))
			throw new AccessControlException("Insufficient permissions to access createapplication");

		CreateApplicationReturnDTO returnValue = new CreateApplicationReturnDTO();
        CreateApplicationCurrentStateDTO currentState = new CreateApplicationCurrentStateDTO();
        
        // Setup the inputs for the first process step
        mapper.map(inputs, currentState.getInputs());
        
		Object returnDTO = null;
		return returnValue;
	}

	/**
	Implements method deleteapplication
	
		Deletes application
	*/
	public DeleteApplicationReturnDTO deleteapplication(SecurityContext securityContext, DeleteApplicationInputParametersDTO inputs)  {

		if (LOGGER.isDebugEnabled())
			LOGGER.debug("Entered method deleteapplication");

		// Do any security checks
		if (securityContext == null)
			throw new AccessControlException("No SecurityContext available so cannot access deleteapplication");

		if (!checkPermissions(securityContext))
			throw new AccessControlException("Insufficient permissions to access deleteapplication");

		DeleteApplicationReturnDTO returnValue = new DeleteApplicationReturnDTO();
        DeleteApplicationCurrentStateDTO currentState = new DeleteApplicationCurrentStateDTO();
        
        // Setup the inputs for the first process step
        mapper.map(inputs, currentState.getInputs());
        
		Object returnDTO = null;
		return returnValue;
	}


    // Supporting Use Case and Process methods

	public abstract GetASpecificApplicationCurrentStateDTO getaspecificapplicationUseCaseStep1(GetASpecificApplicationCurrentStateDTO currentState);
	public abstract GetASpecificApplicationCurrentStateDTO getaspecificapplicationUseCaseStep2(GetASpecificApplicationCurrentStateDTO currentState);
	public abstract GetASpecificApplicationCurrentStateDTO getaspecificapplicationUseCaseStep3(GetASpecificApplicationCurrentStateDTO currentState);
	public abstract GetASpecificApplicationCurrentStateDTO getaspecificapplicationUseCaseStep4(GetASpecificApplicationCurrentStateDTO currentState);
	public abstract GetASpecificApplicationCurrentStateDTO getaspecificapplicationUseCaseStep5(GetASpecificApplicationCurrentStateDTO currentState);
	public abstract GetASpecificApplicationCurrentStateDTO getaspecificapplicationUseCaseStep6(GetASpecificApplicationCurrentStateDTO currentState);


	public abstract FindApplicationCurrentStateDTO findapplicationUseCaseStep1(FindApplicationCurrentStateDTO currentState);
	public abstract FindApplicationCurrentStateDTO findapplicationUseCaseStep2(FindApplicationCurrentStateDTO currentState);
	public abstract FindApplicationCurrentStateDTO findapplicationUseCaseStep3(FindApplicationCurrentStateDTO currentState);
	public abstract FindApplicationCurrentStateDTO findapplicationUseCaseStep4(FindApplicationCurrentStateDTO currentState);
	public abstract FindApplicationCurrentStateDTO findapplicationUseCaseStep5(FindApplicationCurrentStateDTO currentState);
	public abstract FindApplicationCurrentStateDTO findapplicationUseCaseStep6(FindApplicationCurrentStateDTO currentState);


	public abstract UpdateApplicationCurrentStateDTO updateapplicationUseCaseStep1(UpdateApplicationCurrentStateDTO currentState);
	public abstract UpdateApplicationCurrentStateDTO updateapplicationUseCaseStep2(UpdateApplicationCurrentStateDTO currentState);
	public abstract UpdateApplicationCurrentStateDTO updateapplicationUseCaseStep3(UpdateApplicationCurrentStateDTO currentState);
	public abstract UpdateApplicationCurrentStateDTO updateapplicationUseCaseStep4(UpdateApplicationCurrentStateDTO currentState);
	public abstract UpdateApplicationCurrentStateDTO updateapplicationUseCaseStep5(UpdateApplicationCurrentStateDTO currentState);
	public abstract UpdateApplicationCurrentStateDTO updateapplicationUseCaseStep6(UpdateApplicationCurrentStateDTO currentState);
	public abstract UpdateApplicationCurrentStateDTO updateapplicationUseCaseStep7(UpdateApplicationCurrentStateDTO currentState);


	public abstract ReplaceApplicationCurrentStateDTO replaceapplicationUseCaseStep1(ReplaceApplicationCurrentStateDTO currentState);
	public abstract ReplaceApplicationCurrentStateDTO replaceapplicationUseCaseStep2(ReplaceApplicationCurrentStateDTO currentState);
	public abstract ReplaceApplicationCurrentStateDTO replaceapplicationUseCaseStep3(ReplaceApplicationCurrentStateDTO currentState);
	public abstract ReplaceApplicationCurrentStateDTO replaceapplicationUseCaseStep4(ReplaceApplicationCurrentStateDTO currentState);
	public abstract ReplaceApplicationCurrentStateDTO replaceapplicationUseCaseStep5(ReplaceApplicationCurrentStateDTO currentState);
	public abstract ReplaceApplicationCurrentStateDTO replaceapplicationUseCaseStep6(ReplaceApplicationCurrentStateDTO currentState);
	public abstract ReplaceApplicationCurrentStateDTO replaceapplicationUseCaseStep7(ReplaceApplicationCurrentStateDTO currentState);


	public abstract CreateApplicationCurrentStateDTO createapplicationUseCaseStep1(CreateApplicationCurrentStateDTO currentState);
	public abstract CreateApplicationCurrentStateDTO createapplicationUseCaseStep2(CreateApplicationCurrentStateDTO currentState);
	public abstract CreateApplicationCurrentStateDTO createapplicationUseCaseStep3(CreateApplicationCurrentStateDTO currentState);
	public abstract CreateApplicationCurrentStateDTO createapplicationUseCaseStep4(CreateApplicationCurrentStateDTO currentState);
	public abstract CreateApplicationCurrentStateDTO createapplicationUseCaseStep5(CreateApplicationCurrentStateDTO currentState);
	public abstract CreateApplicationCurrentStateDTO createapplicationUseCaseStep6(CreateApplicationCurrentStateDTO currentState);


	public abstract DeleteApplicationCurrentStateDTO deleteapplicationUseCaseStep1(DeleteApplicationCurrentStateDTO currentState);
	public abstract DeleteApplicationCurrentStateDTO deleteapplicationUseCaseStep2(DeleteApplicationCurrentStateDTO currentState);
	public abstract DeleteApplicationCurrentStateDTO deleteapplicationUseCaseStep3(DeleteApplicationCurrentStateDTO currentState);
	public abstract DeleteApplicationCurrentStateDTO deleteapplicationUseCaseStep4(DeleteApplicationCurrentStateDTO currentState);
	public abstract DeleteApplicationCurrentStateDTO deleteapplicationUseCaseStep5(DeleteApplicationCurrentStateDTO currentState);


// Supporting Exception classes

// Supporting DTO classes


	/**
	 * Provides a DTO to hold the current state of the orchestration for the operation Get a specific application.
	 * This allows the state to be easily passed in method calls.
	 */
	public static class GetASpecificApplicationCurrentStateDTO {
		
		private GetASpecificApplicationInputParametersDTO inputs;
		private GetASpecificApplicationReturnDTO returnObject;
		private GetASpecificApplicationReturnStatusDTO errorState;
		// DTOs for orchestration
		
		public GetASpecificApplicationCurrentStateDTO() {
			initialiseDTOs();
		}

		public GetASpecificApplicationCurrentStateDTO(GetASpecificApplicationInputParametersDTO inputs) {
			initialiseDTOs();
			this.inputs = inputs;
		}
		
		// Add extra DTOs for steps
		public GetASpecificApplicationInputParametersDTO getInputs() {
			return inputs;
		}

		
		public void setErrorState(GetASpecificApplicationReturnStatusDTO errorState) {
			this.errorState = errorState;
		}

		public GetASpecificApplicationReturnStatusDTO getErrorState() {
			return errorState;
		}

		public GetASpecificApplicationReturnDTO getReturnObject() {
			return returnObject;
		}
		
		private void initialiseDTOs() {
			inputs = new GetASpecificApplicationInputParametersDTO();
			returnObject = new GetASpecificApplicationReturnDTO();
			errorState = new GetASpecificApplicationReturnStatusDTO();
		
		}			
	};

	/**
	 * Holds the return value for the operation Get a specific application
	 */
	public static class GetASpecificApplicationReturnDTO {
		private com.digitalml.rest.resources.codegentest.Response response;

		public com.digitalml.rest.resources.codegentest.Response getResponse() {
			return response;
		}

		public void setResponse(com.digitalml.rest.resources.codegentest.Response response) {
			this.response = response;
		}

	};

	/**
	 * Holds the return value for the operation Get a specific application when an exception has been thrown.
	 */
	public static class GetASpecificApplicationReturnStatusDTO {

		private String exceptionMessage;

		public GetASpecificApplicationReturnStatusDTO() {
		}

		public GetASpecificApplicationReturnStatusDTO(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}



		public String getExceptionMessage() {
			return exceptionMessage;
		}

		public void setExceptionMessage(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}
	};

	/**
	 * Holds the input parameters for the operation Get a specific application in a single DTO which aids
	 * validation and allows the inputs to be easily passed in method calls.
	 */
	public static class GetASpecificApplicationInputParametersDTO {


		private String id;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}


		public boolean validate() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<GetASpecificApplicationInputParametersDTO>> errors = validator.validate(this);
			return CollectionUtils.isEmpty(errors);
		}

		public List<String> validateReport() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<GetASpecificApplicationInputParametersDTO>> errors = validator.validate(this);

			List<String> results = new ArrayList<String>();
			if (CollectionUtils.isNotEmpty(errors))
				for (ConstraintViolation<GetASpecificApplicationInputParametersDTO> error : errors) {
					StringBuffer sb = new StringBuffer();
					sb.append(error.getMessage());
					results.add(sb.toString());
				}

			return results;
		}
	};


	/**
	 * Provides a DTO to hold the current state of the orchestration for the operation Find application.
	 * This allows the state to be easily passed in method calls.
	 */
	public static class FindApplicationCurrentStateDTO {
		
		private FindApplicationInputParametersDTO inputs;
		private FindApplicationReturnDTO returnObject;
		private FindApplicationReturnStatusDTO errorState;
		// DTOs for orchestration
		
		public FindApplicationCurrentStateDTO() {
			initialiseDTOs();
		}

		public FindApplicationCurrentStateDTO(FindApplicationInputParametersDTO inputs) {
			initialiseDTOs();
			this.inputs = inputs;
		}
		
		// Add extra DTOs for steps
		public FindApplicationInputParametersDTO getInputs() {
			return inputs;
		}

		
		public void setErrorState(FindApplicationReturnStatusDTO errorState) {
			this.errorState = errorState;
		}

		public FindApplicationReturnStatusDTO getErrorState() {
			return errorState;
		}

		public FindApplicationReturnDTO getReturnObject() {
			return returnObject;
		}
		
		private void initialiseDTOs() {
			inputs = new FindApplicationInputParametersDTO();
			returnObject = new FindApplicationReturnDTO();
			errorState = new FindApplicationReturnStatusDTO();
		
		}			
	};

	/**
	 * Holds the return value for the operation Find application
	 */
	public static class FindApplicationReturnDTO {
		private com.digitalml.rest.resources.codegentest.Response response;

		public com.digitalml.rest.resources.codegentest.Response getResponse() {
			return response;
		}

		public void setResponse(com.digitalml.rest.resources.codegentest.Response response) {
			this.response = response;
		}

	};

	/**
	 * Holds the return value for the operation Find application when an exception has been thrown.
	 */
	public static class FindApplicationReturnStatusDTO {

		private String exceptionMessage;

		public FindApplicationReturnStatusDTO() {
		}

		public FindApplicationReturnStatusDTO(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}



		public String getExceptionMessage() {
			return exceptionMessage;
		}

		public void setExceptionMessage(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}
	};

	/**
	 * Holds the input parameters for the operation Find application in a single DTO which aids
	 * validation and allows the inputs to be easily passed in method calls.
	 */
	public static class FindApplicationInputParametersDTO {


		private Integer offset;

		private Integer pagesize;

		public Integer getOffset() {
			return offset;
		}

		public void setOffset(Integer offset) {
			this.offset = offset;
		}

		public Integer getPagesize() {
			return pagesize;
		}

		public void setPagesize(Integer pagesize) {
			this.pagesize = pagesize;
		}


		public boolean validate() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<FindApplicationInputParametersDTO>> errors = validator.validate(this);
			return CollectionUtils.isEmpty(errors);
		}

		public List<String> validateReport() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<FindApplicationInputParametersDTO>> errors = validator.validate(this);

			List<String> results = new ArrayList<String>();
			if (CollectionUtils.isNotEmpty(errors))
				for (ConstraintViolation<FindApplicationInputParametersDTO> error : errors) {
					StringBuffer sb = new StringBuffer();
					sb.append(error.getMessage());
					results.add(sb.toString());
				}

			return results;
		}
	};


	/**
	 * Provides a DTO to hold the current state of the orchestration for the operation Update application.
	 * This allows the state to be easily passed in method calls.
	 */
	public static class UpdateApplicationCurrentStateDTO {
		
		private UpdateApplicationInputParametersDTO inputs;
		private UpdateApplicationReturnDTO returnObject;
		private UpdateApplicationReturnStatusDTO errorState;
		// DTOs for orchestration
		
		public UpdateApplicationCurrentStateDTO() {
			initialiseDTOs();
		}

		public UpdateApplicationCurrentStateDTO(UpdateApplicationInputParametersDTO inputs) {
			initialiseDTOs();
			this.inputs = inputs;
		}
		
		// Add extra DTOs for steps
		public UpdateApplicationInputParametersDTO getInputs() {
			return inputs;
		}

		
		public void setErrorState(UpdateApplicationReturnStatusDTO errorState) {
			this.errorState = errorState;
		}

		public UpdateApplicationReturnStatusDTO getErrorState() {
			return errorState;
		}

		public UpdateApplicationReturnDTO getReturnObject() {
			return returnObject;
		}
		
		private void initialiseDTOs() {
			inputs = new UpdateApplicationInputParametersDTO();
			returnObject = new UpdateApplicationReturnDTO();
			errorState = new UpdateApplicationReturnStatusDTO();
		
		}			
	};

	/**
	 * Holds the return value for the operation Update application
	 */
	public static class UpdateApplicationReturnDTO {

	};

	/**
	 * Holds the return value for the operation Update application when an exception has been thrown.
	 */
	public static class UpdateApplicationReturnStatusDTO {

		private String exceptionMessage;

		public UpdateApplicationReturnStatusDTO() {
		}

		public UpdateApplicationReturnStatusDTO(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}



		public String getExceptionMessage() {
			return exceptionMessage;
		}

		public void setExceptionMessage(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}
	};

	/**
	 * Holds the input parameters for the operation Update application in a single DTO which aids
	 * validation and allows the inputs to be easily passed in method calls.
	 */
	public static class UpdateApplicationInputParametersDTO {


    	@NotNull
		private com.digitalml.rest.resources.codegentest.Request request;

		public com.digitalml.rest.resources.codegentest.Request getRequest() {
			return request;
		}

		public void setRequest(com.digitalml.rest.resources.codegentest.Request request) {
			this.request = request;
		}


		public boolean validate() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<UpdateApplicationInputParametersDTO>> errors = validator.validate(this);
			return CollectionUtils.isEmpty(errors);
		}

		public List<String> validateReport() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<UpdateApplicationInputParametersDTO>> errors = validator.validate(this);

			List<String> results = new ArrayList<String>();
			if (CollectionUtils.isNotEmpty(errors))
				for (ConstraintViolation<UpdateApplicationInputParametersDTO> error : errors) {
					StringBuffer sb = new StringBuffer();
					sb.append(error.getMessage());
					results.add(sb.toString());
				}

			return results;
		}
	};


	/**
	 * Provides a DTO to hold the current state of the orchestration for the operation Replace application.
	 * This allows the state to be easily passed in method calls.
	 */
	public static class ReplaceApplicationCurrentStateDTO {
		
		private ReplaceApplicationInputParametersDTO inputs;
		private ReplaceApplicationReturnDTO returnObject;
		private ReplaceApplicationReturnStatusDTO errorState;
		// DTOs for orchestration
		
		public ReplaceApplicationCurrentStateDTO() {
			initialiseDTOs();
		}

		public ReplaceApplicationCurrentStateDTO(ReplaceApplicationInputParametersDTO inputs) {
			initialiseDTOs();
			this.inputs = inputs;
		}
		
		// Add extra DTOs for steps
		public ReplaceApplicationInputParametersDTO getInputs() {
			return inputs;
		}

		
		public void setErrorState(ReplaceApplicationReturnStatusDTO errorState) {
			this.errorState = errorState;
		}

		public ReplaceApplicationReturnStatusDTO getErrorState() {
			return errorState;
		}

		public ReplaceApplicationReturnDTO getReturnObject() {
			return returnObject;
		}
		
		private void initialiseDTOs() {
			inputs = new ReplaceApplicationInputParametersDTO();
			returnObject = new ReplaceApplicationReturnDTO();
			errorState = new ReplaceApplicationReturnStatusDTO();
		
		}			
	};

	/**
	 * Holds the return value for the operation Replace application
	 */
	public static class ReplaceApplicationReturnDTO {

	};

	/**
	 * Holds the return value for the operation Replace application when an exception has been thrown.
	 */
	public static class ReplaceApplicationReturnStatusDTO {

		private String exceptionMessage;

		public ReplaceApplicationReturnStatusDTO() {
		}

		public ReplaceApplicationReturnStatusDTO(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}



		public String getExceptionMessage() {
			return exceptionMessage;
		}

		public void setExceptionMessage(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}
	};

	/**
	 * Holds the input parameters for the operation Replace application in a single DTO which aids
	 * validation and allows the inputs to be easily passed in method calls.
	 */
	public static class ReplaceApplicationInputParametersDTO {


    	@NotNull
		private com.digitalml.rest.resources.codegentest.Request request;

		public com.digitalml.rest.resources.codegentest.Request getRequest() {
			return request;
		}

		public void setRequest(com.digitalml.rest.resources.codegentest.Request request) {
			this.request = request;
		}


		public boolean validate() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<ReplaceApplicationInputParametersDTO>> errors = validator.validate(this);
			return CollectionUtils.isEmpty(errors);
		}

		public List<String> validateReport() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<ReplaceApplicationInputParametersDTO>> errors = validator.validate(this);

			List<String> results = new ArrayList<String>();
			if (CollectionUtils.isNotEmpty(errors))
				for (ConstraintViolation<ReplaceApplicationInputParametersDTO> error : errors) {
					StringBuffer sb = new StringBuffer();
					sb.append(error.getMessage());
					results.add(sb.toString());
				}

			return results;
		}
	};


	/**
	 * Provides a DTO to hold the current state of the orchestration for the operation Create application.
	 * This allows the state to be easily passed in method calls.
	 */
	public static class CreateApplicationCurrentStateDTO {
		
		private CreateApplicationInputParametersDTO inputs;
		private CreateApplicationReturnDTO returnObject;
		private CreateApplicationReturnStatusDTO errorState;
		// DTOs for orchestration
		
		public CreateApplicationCurrentStateDTO() {
			initialiseDTOs();
		}

		public CreateApplicationCurrentStateDTO(CreateApplicationInputParametersDTO inputs) {
			initialiseDTOs();
			this.inputs = inputs;
		}
		
		// Add extra DTOs for steps
		public CreateApplicationInputParametersDTO getInputs() {
			return inputs;
		}

		
		public void setErrorState(CreateApplicationReturnStatusDTO errorState) {
			this.errorState = errorState;
		}

		public CreateApplicationReturnStatusDTO getErrorState() {
			return errorState;
		}

		public CreateApplicationReturnDTO getReturnObject() {
			return returnObject;
		}
		
		private void initialiseDTOs() {
			inputs = new CreateApplicationInputParametersDTO();
			returnObject = new CreateApplicationReturnDTO();
			errorState = new CreateApplicationReturnStatusDTO();
		
		}			
	};

	/**
	 * Holds the return value for the operation Create application
	 */
	public static class CreateApplicationReturnDTO {
		private com.digitalml.rest.resources.codegentest.Response response;

		public com.digitalml.rest.resources.codegentest.Response getResponse() {
			return response;
		}

		public void setResponse(com.digitalml.rest.resources.codegentest.Response response) {
			this.response = response;
		}

	};

	/**
	 * Holds the return value for the operation Create application when an exception has been thrown.
	 */
	public static class CreateApplicationReturnStatusDTO {

		private String exceptionMessage;

		public CreateApplicationReturnStatusDTO() {
		}

		public CreateApplicationReturnStatusDTO(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}



		public String getExceptionMessage() {
			return exceptionMessage;
		}

		public void setExceptionMessage(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}
	};

	/**
	 * Holds the input parameters for the operation Create application in a single DTO which aids
	 * validation and allows the inputs to be easily passed in method calls.
	 */
	public static class CreateApplicationInputParametersDTO {


    	@NotNull
		private com.digitalml.rest.resources.codegentest.Request request;

		public com.digitalml.rest.resources.codegentest.Request getRequest() {
			return request;
		}

		public void setRequest(com.digitalml.rest.resources.codegentest.Request request) {
			this.request = request;
		}


		public boolean validate() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<CreateApplicationInputParametersDTO>> errors = validator.validate(this);
			return CollectionUtils.isEmpty(errors);
		}

		public List<String> validateReport() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<CreateApplicationInputParametersDTO>> errors = validator.validate(this);

			List<String> results = new ArrayList<String>();
			if (CollectionUtils.isNotEmpty(errors))
				for (ConstraintViolation<CreateApplicationInputParametersDTO> error : errors) {
					StringBuffer sb = new StringBuffer();
					sb.append(error.getMessage());
					results.add(sb.toString());
				}

			return results;
		}
	};


	/**
	 * Provides a DTO to hold the current state of the orchestration for the operation Delete application.
	 * This allows the state to be easily passed in method calls.
	 */
	public static class DeleteApplicationCurrentStateDTO {
		
		private DeleteApplicationInputParametersDTO inputs;
		private DeleteApplicationReturnDTO returnObject;
		private DeleteApplicationReturnStatusDTO errorState;
		// DTOs for orchestration
		
		public DeleteApplicationCurrentStateDTO() {
			initialiseDTOs();
		}

		public DeleteApplicationCurrentStateDTO(DeleteApplicationInputParametersDTO inputs) {
			initialiseDTOs();
			this.inputs = inputs;
		}
		
		// Add extra DTOs for steps
		public DeleteApplicationInputParametersDTO getInputs() {
			return inputs;
		}

		
		public void setErrorState(DeleteApplicationReturnStatusDTO errorState) {
			this.errorState = errorState;
		}

		public DeleteApplicationReturnStatusDTO getErrorState() {
			return errorState;
		}

		public DeleteApplicationReturnDTO getReturnObject() {
			return returnObject;
		}
		
		private void initialiseDTOs() {
			inputs = new DeleteApplicationInputParametersDTO();
			returnObject = new DeleteApplicationReturnDTO();
			errorState = new DeleteApplicationReturnStatusDTO();
		
		}			
	};

	/**
	 * Holds the return value for the operation Delete application
	 */
	public static class DeleteApplicationReturnDTO {

	};

	/**
	 * Holds the return value for the operation Delete application when an exception has been thrown.
	 */
	public static class DeleteApplicationReturnStatusDTO {

		private String exceptionMessage;

		public DeleteApplicationReturnStatusDTO() {
		}

		public DeleteApplicationReturnStatusDTO(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}



		public String getExceptionMessage() {
			return exceptionMessage;
		}

		public void setExceptionMessage(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}
	};

	/**
	 * Holds the input parameters for the operation Delete application in a single DTO which aids
	 * validation and allows the inputs to be easily passed in method calls.
	 */
	public static class DeleteApplicationInputParametersDTO {


		private String id;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}


		public boolean validate() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<DeleteApplicationInputParametersDTO>> errors = validator.validate(this);
			return CollectionUtils.isEmpty(errors);
		}

		public List<String> validateReport() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<DeleteApplicationInputParametersDTO>> errors = validator.validate(this);

			List<String> results = new ArrayList<String>();
			if (CollectionUtils.isNotEmpty(errors))
				for (ConstraintViolation<DeleteApplicationInputParametersDTO> error : errors) {
					StringBuffer sb = new StringBuffer();
					sb.append(error.getMessage());
					results.add(sb.toString());
				}

			return results;
		}
	};

}