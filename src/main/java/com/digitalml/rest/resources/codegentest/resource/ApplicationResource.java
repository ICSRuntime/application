package com.digitalml.rest.resources.codegentest.resource;
        	
import java.lang.IllegalArgumentException;
import java.security.AccessControlException;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import io.dropwizard.jersey.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
	
// Customer specific imports

// Service specific imports
import com.digitalml.rest.resources.codegentest.service.ApplicationService;
	
import com.digitalml.rest.resources.codegentest.service.ApplicationService.GetASpecificApplicationReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.GetASpecificApplicationReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.GetASpecificApplicationInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.FindApplicationReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.FindApplicationReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.FindApplicationInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.UpdateApplicationReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.UpdateApplicationReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.UpdateApplicationInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.ReplaceApplicationReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.ReplaceApplicationReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.ReplaceApplicationInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.CreateApplicationReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.CreateApplicationReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.CreateApplicationInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.DeleteApplicationReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.DeleteApplicationReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ApplicationService.DeleteApplicationInputParametersDTO;
	
import com.digitalml.rest.resources.codegentest.*;
	
	/**
	 * Service: Application
	 * Access and update of insurance application information.
	 *
	 * @author admin
	 * @version 1.0
	 *
	 */
	
	@Path("http://digitalml.com/ICS/rest")
		@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public class ApplicationResource {
		
	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationResource.class);
	
	@Context
	private SecurityContext securityContext;

	@Context
	private javax.ws.rs.core.Request request;

	@Context
	private HttpServletRequest httpRequest;

	private ApplicationService delegateService;

	private String implementationClass = "com.digitalml.rest.resources.codegentest.service.Application.ApplicationServiceDefaultImpl";

	public void setImplementationClass(String className) {
		implementationClass = className;
	}

	public void setImplementationClass(Class clazz) {
		if (clazz == null)
			throw new IllegalArgumentException("Parameter clazz cannot be null");

		implementationClass = clazz.getName();
	}
		
	private ApplicationService getCurrentImplementation() {

		Object object = null;
		try {
			Class c = Class.forName(implementationClass, true, Thread.currentThread().getContextClassLoader());
			object = c.newInstance();

		} catch (ClassNotFoundException exc) {
			LOGGER.error(implementationClass + " not found");
			return null;

		} catch (IllegalAccessException exc) {
			LOGGER.error("Cannot access " + implementationClass);
			return null;

		} catch (InstantiationException exc) {
			LOGGER.error("Cannot instantiate " + implementationClass);
			return null;
		}

		if (!(object instanceof ApplicationService)) {
			LOGGER.error(implementationClass + " is not an instance of " + ApplicationService.class.getName());
			return null;
		}

		return (ApplicationService)object;
	}
	
	{
		delegateService = getCurrentImplementation();
	}
	
	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}


	/**
	Method: getaspecificapplication
		Gets application details

	Non-functional requirements:
	*/
	
	@GET
	@Path("/application/{id}")
	public javax.ws.rs.core.Response getaspecificapplication(
		@PathParam("id")@NotEmpty String id) {

		GetASpecificApplicationInputParametersDTO inputs = new ApplicationService.GetASpecificApplicationInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			GetASpecificApplicationReturnDTO returnValue = delegateService.getaspecificapplication(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: findapplication
		Gets a collection of application details filtered 

	Non-functional requirements:
	*/
	
	@GET
	@Path("/application")
	public javax.ws.rs.core.Response findapplication(
		@QueryParam("offset") Integer offset,
		@QueryParam("pagesize") Integer pagesize) {

		FindApplicationInputParametersDTO inputs = new ApplicationService.FindApplicationInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setOffset(offset);
		inputs.setPagesize(pagesize);
	
		try {
			FindApplicationReturnDTO returnValue = delegateService.findapplication(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateapplication
		Updates application

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/application/{id}")
	public javax.ws.rs.core.Response updateapplication(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		UpdateApplicationInputParametersDTO inputs = new ApplicationService.UpdateApplicationInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			UpdateApplicationReturnDTO returnValue = delegateService.updateapplication(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: replaceapplication
		Replaces application

	Non-functional requirements:
	*/
	
	@PUT
	@Path("/application/{id}")
	public javax.ws.rs.core.Response replaceapplication(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		ReplaceApplicationInputParametersDTO inputs = new ApplicationService.ReplaceApplicationInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			ReplaceApplicationReturnDTO returnValue = delegateService.replaceapplication(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createapplication
		Creates application

	Non-functional requirements:
	*/
	
	@POST
	@Path("/application")
	public javax.ws.rs.core.Response createapplication(
		 com.digitalml.rest.resources.codegentest.Request Request) {

		CreateApplicationInputParametersDTO inputs = new ApplicationService.CreateApplicationInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setRequest(Request);
	
		try {
			CreateApplicationReturnDTO returnValue = delegateService.createapplication(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deleteapplication
		Deletes application

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/application/{id}")
	public javax.ws.rs.core.Response deleteapplication(
		@PathParam("id")@NotEmpty String id) {

		DeleteApplicationInputParametersDTO inputs = new ApplicationService.DeleteApplicationInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			DeleteApplicationReturnDTO returnValue = delegateService.deleteapplication(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
}