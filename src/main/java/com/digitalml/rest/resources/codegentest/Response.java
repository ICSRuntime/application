package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Response:
{
  "required": [
    "application"
  ],
  "type": "object",
  "properties": {
    "application": {
      "$ref": "Application"
    }
  }
}
*/

public class Response {

	@Size(max=1)
	@NotNull
	private com.digitalml.insurance.application.Application application;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    application = new com.digitalml.insurance.application.Application();
	}
	public com.digitalml.insurance.application.Application getApplication() {
		return application;
	}
	
	public void setApplication(com.digitalml.insurance.application.Application application) {
		this.application = application;
	}
}